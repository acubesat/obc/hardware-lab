#include <catch2/catch.hpp>
#include <iostream>

// An example test case. Test cases are parts of code that are executed separately and have a title as shown below.
// For more information, read https://github.com/catchorg/Catch2/blob/master/docs/test-cases-and-sections.md

TEST_CASE("Example test case", "") {
    // A quick function to calculate pi
    // Suggestion: Use a large number of loops so that the tests last long enough for them to be measurable.
    // The microcontroller may be 30-3000 times slower than your PC.
    float pi = 0.0;
    int sign = 1;
    for (uint64_t i = 0; i < 1000000; ++i) {
        pi += 4.0f * sign / (2.0f * i + 1.0f);
        sign *= -1;
    }

    // Assertions are a way to calculate if a test is successful or not. Normally, in a test all assertions must be
    // true. Here we validate that pi is indeed larger than 3.14. If an assertion fails, you will see the infamous
    // "build failed" message.
    // Assertions are not necessary to do benchmarks or other MCU experiments, but they are quite useful to check
    // if your microcontroller is not doing something completely wrong.
    // For more information, read https://github.com/catchorg/Catch2/blob/master/docs/assertions.md
    CHECK(pi > 3.14);
    CHECK(pi < 3.15);

    // You can also print, like you normally would with C++!
    std::cout << "Would you expect that? pi is " << pi << "!" << std::endl;
}
