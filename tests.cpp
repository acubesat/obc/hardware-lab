// This is the main test file, the main entry point of the Catch2 test application.
// This file is only used when running in desktop (x86), not on the microcontroller.
// You should just leave it here, and write your tests in the `test` folder instead.
//
// This file is defining a custom main() function, in order to show test durations
// by default.

#define CATCH_CONFIG_RUNNER

#include "catch2/catch.hpp"

int main(int argc, char *argv[]) {
    Catch::Session session; // There must be exactly one instance

    // writing to session.configData() here sets defaults
    // this is the preferred way to set them
    session.configData().showDurations = Catch::ShowDurations::Always;

    int returnCode = session.applyCommandLine(argc, argv);
    if (returnCode != 0) { // Indicates a command line error
        return returnCode;
    }

    int numFailed = session.run();

    // numFailed is clamped to 255 as some unices only use the lower 8 bits.
    // This clamping has already been applied, so just return it here
    // You can also do any post run clean-up here
    return numFailed;
}
