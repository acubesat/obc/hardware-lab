cmake_minimum_required(VERSION 3.7)
project(ecss_services)

# Set C++ version to c++17
set(CMAKE_CXX_STANDARD 17)

# Specify the directories for #includes
include_directories("${PROJECT_SOURCE_DIR}/inc" "${PROJECT_SOURCE_DIR}/lib/etl/include"
        "${PROJECT_SOURCE_DIR}/lib/Catch2/single_include")

# Specify the .cpp files common across all targets
file(GLOB_RECURSE common_SRC "src/*.cpp")
add_library(common OBJECT ${common_SRC})

# Specify the .cpp files for cross-platform tests
file(GLOB_RECURSE test_common_SRC "test/*.cpp")
add_library(test_common OBJECT ${test_common_SRC})

# Include Catch2 dependencies
add_subdirectory(lib/Catch2)

# Create the user-runnable tests executable
add_executable(tests "tests.cpp")
target_link_libraries(tests common test_common Catch2::Catch2)
