# Remote Hardware Lab

**What is this?**  
This repository allows you to **test** and **benchmark** microcontroller code without
leaving the comfort of your home!

Any commits done on this repository will be **automatically uploaded on the [*hardware
testing platform*](https://gitlab.com/acubesat/obc/hil-tests) and tested** on an actual
microcontroller.

You only need to write the code!

### What does it look like?
![Screenshot](screenshot.png)

You can look at examples of the Remote Hardware Lab in action by browsing the
[**CI/CD**](https://gitlab.com/acubesat/obc/hardware-lab/-/pipelines) jobs. Note that
tests run both on a desktop (`test-sw`) and on the hardware (`test-hw`).

### How to make it work?
1. Create a new branch to work on
2. Add any libraries or code you want in the `inc`, `lib` and `src` folders.
3. There is no `main()` function, but there are **tests** located in the `test`
   directory. Tests:
     - Automatically have their duration calculated
     - Can fail or succeed
     - Can output text that will be displayed by the MCU
   
   For more information, look at the [Catch2 tutorial](https://github.com/catchorg/Catch2/blob/master/docs/tutorial.md#writing-tests).

   As part of this step, you must replace everything in the `test` directory with
   **your own homebrew tests**! These tests will define everything you want to be
   executed. Consider them your `main()` function.
   
   As soon as you are done with tests, you should be able to have something that
   compiles on your desktop and says "All tests passed".
4. Just commit your changes!

   The testing platform will upload your code to the MCU automatically, and you should
   see the (hopefully successful) results of your changes in the _CI/CD_ menu option.

### How to compile on my desktop?
You'll need the essential compiler software, usually attainable with
`sudo apt install build-essential cmake`.

Also, don't forget to clone all **submodules** of this repository — they include
Catch2, the most significant component of all.

```bash
# Create build directory
mkdir build
cd build

# Compile the software
cmake ..
make

# Run the tests
./tests
```

### Tips, tricks & advice
- Something running fast on your computer may be 30-3000 times slower on a microcontroller.
  Plan accordingly.
- Pay attention to the support of `float`s and `double`s for the MCU you are using.
  For example, the STM32L4+ doesn't support `double`s in hardware, making a `float`
  implementation much faster.
- Don't print stuff too often — sending data via UART takes up precious time and
  will skew your test results!
- Pay attention to memory (both ROM & RAM) — microcontrollers have very limited
  resources!
- Instead of just measuring time of execution, it is useful to add a few `CHECK`s or
  prints to ensure your microcontroller doesn't return jumbled up results.
- Consider that `int`s and `uint`s may have different size on the MCU.
